# Teste aliança - Verzo

Teste de habilidades: Desenvolver uma aplicação para calculo de descontos em medicamentos.

Para testar a aplicação, acessar: <https://verzo-teste-alianca.netlify.com/>

## Estrutura de Pastas

    .
    ├── README.md
    ├── package.json
    ├── .eslintrc.js
    ├── .editorconfig
    ├── node_modules
    ├── public
    └── src
        ├── views
        │   └── Discount.js
        │   └── components
        │       └── DiscountForm.js
        │       └── DiscountTable.js
        ├── helpers
        ├── routes
        ├── stores
        ├── App.js
        ├── index.js
        └── serviceWorker.js

## Development

```bash
git clone git@bitbucket.org:thiago_mendes/alianca-medicamentos.git
cd alianca-medicamentos
yarn install
yarn start
```

Acesse: <http://127.0.0.1:3000> ou <http://localhost:3000>

## Scripts

In the project directory, you can run:

### `yarn start`

Roda o projeto em modo de desenvolvimento.

### `yarn build`

Gera os arquivos para produção dentro da pasta `Build`.

### `yarn test`

Roda os tests da aplicação.

### Netlify

Ferramenta para otimizar o processo de validação do teste

> Tudo que for produzido na branch `master` será disponibilizado na url <https://verzo-teste-alianca.netlify.com/>

## Bibliotecas e Ferramentas Utilizadas

- [Create React App](https://github.com/facebookincubator/create-react-app)
- [React Router](https://reacttraining.com/react-router/web/guides/philosophy)
- [Ant Design](https://ant.design/)
- [Apollo](https://www.apollographql.com/)
- [Netlify](https://www.netlify.com/)
