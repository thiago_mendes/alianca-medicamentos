import React from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { Icon } from "antd";
import styled from "styled-components";

const Loading = styled.div`
  min-height: 102px;
  font-size: 40px;
  text-align: center;
  line-height: 102px;
  color: #ccc;
`;

const GET_MEDICAMENTOS = gql`
  query listMedicamentos {
    listMedicamentos {
      items {
        id
        name
        limit
        percent
        value
      }
    }
  }
`;

const ListMedicamentosQuery = ({ children }) => (
  <Query query={GET_MEDICAMENTOS}>
    {({ loading, data }) => {
      if (loading)
        return (
          <Loading>
            {" "}
            <Icon type="sync" spin />
          </Loading>
        );

      if (data) {
        const {
          listMedicamentos: { items }
        } = data;

        return children(items);
      }
    }}
  </Query>
);

export default ListMedicamentosQuery;
