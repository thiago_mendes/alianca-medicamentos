import gql from "graphql-tag";

const GET_REEMBOLSO = gql`
  query calcReembolso($id: ID!, $quantidade: Int!) {
    calcReembolso(id: $id, quantidade: $quantidade)
  }
`;

export default GET_REEMBOLSO;
