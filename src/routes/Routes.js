import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Discount from "../views/Discount";

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Discount} />
    </Switch>
  </Router>
);

export default Routes;
