import React from "react";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import renderer from "react-test-renderer";
import Config from "../Config";
import Discount from "./Discount";

const client = new ApolloClient({
  uri: Config.apiUri,
  headers: { "x-api-key": "da2-lxftgw43gfhkjeoklcj3sdzsxi" }
});

// Not predictable
it("renders without error", () => {
  const Main = renderer
    .create(
      <ApolloProvider client={client}>
        <Discount />
      </ApolloProvider>
    )
    .toJSON();
  expect(Main).toMatchSnapshot();
});
