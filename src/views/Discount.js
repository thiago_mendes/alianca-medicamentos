import React, { Component } from "react";
import { Row, Col } from "antd";

import styled from "styled-components";

import uuid from "../helpers/utils";
import DiscountForm from "./components/DiscountForm";
import DiscountTable from "./components/DiscountTable";

const Main = styled.section`
  width: 90%;
  max-width: 1280px;
  margin: 20px auto;
`;

const HeaderPage = styled.header`
  background: #8e00f6;
  text-align: center;
  color: #fff;
  font-size: 36px;
  padding: 20px;
  font-weight: bold;
`;

const TitlePage = styled.h1`
  margin: 50px 0;
  font-size: 34px;
`;

const ColTop = styled(Col)`
  margin: 0 0 50px;
`;

class Discount extends Component {
  state = {
    historico: []
  };

  componentDidMount() {
    const getInfo = localStorage.getItem("historico");
    if (getInfo) {
      this.setState({
        historico: JSON.parse(getInfo)
      });
    }
  }

  addItemHistorico = consultaItem => {
    const { historico } = this.state;
    this.setState({
      historico: [
        {
          ...consultaItem,
          uuid: uuid()
        },
        ...historico
      ]
    });

    const historicoBd = [
      {
        ...consultaItem,
        uuid: uuid()
      },
      ...historico
    ];

    localStorage.setItem("historico", JSON.stringify(historicoBd));
  };
  render() {
    const { historico } = this.state;

    return (
      <Main>
        <HeaderPage>Verzo</HeaderPage>
        <TitlePage>Consulta de Medicamentos</TitlePage>
        <Row gutter={24}>
          <ColTop xl={8} lg={10}>
            <DiscountForm addConsulta={item => this.addItemHistorico(item)} />
          </ColTop>
          <Col xl={16} lg={14}>
            <DiscountTable historico={historico} />
          </Col>
        </Row>
      </Main>
    );
  }
}

export default Discount;
