import React, { Component, Fragment } from "react";
import { ApolloConsumer } from "react-apollo";
import { Form, Input, Select, Button, Row, Col, Card } from "antd";
import numeral from "numeral";
import PropTypes from "prop-types";
import styled from "styled-components";
import ListMedicamentosQuery from "../../stores/queries/ListMedicamentosQuery";
import GET_REEMBOLSO from "../../stores/queries/CalcReembolsoQuery";

import "numeral/locales";

const FormItem = Form.Item;
const Option = Select.Option;

numeral.locale("pt-br");

const ButtonCalcular = styled(Button)`
  &.ant-btn-primary {
    float: right;
    background: #8e00f6;
    border-color: #8e00f6;
    width: 150px;
  }
`;

const BoxValor = styled.div`
  background: #fafafa;
  width: 100%;
  padding: 20px;
  font-size: 26px;
  color: #8e00f6;
  margin-top: 20px;
  overflor: hidden;
  span {
    float: right;
  }
`;

class DiscountForm extends Component {
  state = {
    consulta: {},
    medicamento: {},
    valueReembolso: "0.0"
  };

  renderOptions = items => {
    return items.map(item => {
      const { name, id } = item;
      return (
        <Option key={id} medicamento={{ id, name }}>
          {name}
        </Option>
      );
    });
  };

  handleSubmit = (e, client) => {
    e.preventDefault();

    const { form } = this.props;
    form.validateFields((err, values) => {
      if (!err) {
        const {
          medicamento: { id, name }
        } = this.state;

        this.setState(
          {
            consulta: {
              id,
              quantidade: values.quantidade,
              name
            }
          },
          () => this.getCalReembolso(client)
        );
      }
    });
  };

  getCalReembolso = async client => {
    const { consulta } = this.state;
    const {
      data: { calcReembolso }
    } = await client.query({
      query: GET_REEMBOLSO,
      variables: { ...consulta }
    });

    this.setState(
      {
        valueReembolso: calcReembolso
      },
      () => this.addHistorico(consulta, calcReembolso)
    );
  };

  addHistorico = (item, calcReembolso) => {
    const reembolso = calcReembolso || "0.0";
    const historicoItem = {
      ...item,
      valor: numeral(reembolso.replace(".", ",")).format("$ 0,0.00")
    };
    const { addConsulta } = this.props;

    addConsulta(historicoItem);
  };

  render() {
    const {
      form: { getFieldDecorator }
    } = this.props;
    const { valueReembolso } = this.state;

    return (
      <ApolloConsumer>
        {client => (
          <Fragment>
            <Card type="inner" title="Faça a sua consulta">
              <Form
                onSubmit={e => {
                  this.handleSubmit(e, client);
                }}
              >
                <ListMedicamentosQuery>
                  {items => (
                    <Row gutter={16}>
                      <Col sm={14}>
                        <FormItem label="Medicamento">
                          {getFieldDecorator("medicamento", {
                            rules: [
                              { required: true, message: "Campo obrigatório!" }
                            ]
                          })(
                            <Select
                              onSelect={(value, option) => {
                                this.setState({
                                  medicamento: { ...option.props.medicamento }
                                });
                              }}
                            >
                              {this.renderOptions(items)}
                            </Select>
                          )}
                        </FormItem>
                      </Col>
                      <Col sm={10}>
                        <FormItem label="Quantidade">
                          {getFieldDecorator("quantidade", {
                            rules: [
                              { required: true, message: "Campo obrigatório!" }
                            ]
                          })(<Input type="number" />)}
                        </FormItem>
                      </Col>
                    </Row>
                  )}
                </ListMedicamentosQuery>
                <Row>
                  <Col sm={24}>
                    <ButtonCalcular type="primary" htmlType="submit">
                      Consultar
                    </ButtonCalcular>
                  </Col>
                </Row>
                <Row>
                  <Col sm={24}>
                    <div>
                      <BoxValor>
                        Valor:{" "}
                        <span>
                          {numeral(valueReembolso.replace(".", ",")).format(
                            "$ 0,0.00"
                          )}
                        </span>
                      </BoxValor>
                    </div>
                  </Col>
                </Row>
              </Form>
            </Card>
          </Fragment>
        )}
      </ApolloConsumer>
    );
  }
}

DiscountForm.propTypes = {
  addConsulta: PropTypes.func,
  form: PropTypes.shape().isRequired
};

DiscountForm.defaultProps = {
  addConsulta: () => {}
};

export default Form.create()(DiscountForm);
