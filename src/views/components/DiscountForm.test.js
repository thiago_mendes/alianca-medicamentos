import React from "react";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import renderer from "react-test-renderer";
import Config from "../../Config";
import DiscountForm from "./DiscountForm";

const client = new ApolloClient({
  uri: Config.apiUri,
  headers: { "x-api-key": "da2-lxftgw43gfhkjeoklcj3sdzsxi" }
});

it("Render DiscountForm - Compare component versions", () => {
  const Main = renderer
    .create(
      <ApolloProvider client={client}>
        <DiscountForm />
      </ApolloProvider>
    )
    .toJSON();
  expect(Main).toMatchSnapshot();
});
