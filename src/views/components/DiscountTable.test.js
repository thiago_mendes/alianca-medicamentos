import React from "react";
import renderer from "react-test-renderer";
import DiscountTable from "./DiscountTable";

it("Render DiscountTable - Compare component versions", () => {
  const Main = renderer.create(<DiscountTable />).toJSON();
  expect(Main).toMatchSnapshot();
});
