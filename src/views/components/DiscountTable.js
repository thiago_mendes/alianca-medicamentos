import React from "react";
import { Table } from "antd";
import PropTypes from "prop-types";

const columns = [
  {
    title: "Medicamento",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Quantidade",
    dataIndex: "quantidade",
    key: "qtn"
  },
  {
    title: "Valor",
    dataIndex: "valor",
    key: "valor"
  }
];

const discountTable = ({ historico }) => {
  return (
    <Table
      columns={columns}
      dataSource={historico}
      rowKey={historico => historico.uuid}
      bordered
      title={() => "Histórico de Consultas"}
      locale={{ emptyText: "Nenhuma consulta registrada." }}
    />
  );
};

discountTable.propTypes = {
  historico: PropTypes.arrayOf(PropTypes.shape())
};

discountTable.defaultProps = {
  historico: []
};

export default discountTable;
