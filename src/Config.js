let env =
  process.env.DASH_ENV ||
  process.env.REACT_APP_ENV_NAME ||
  process.env.NODE_ENV;

const validEnvs = "development production staging test".split(" ");
const validEnv = validEnvs.find(v => v === env);
env = validEnv || "development";

const Config = Object.assign({}, process.env, {
  env,
  apiUri:
    process.env.REACT_APP_API_URI ||
    "https://m67yacrhlbf7dpxlccdgr7dy4i.appsync-api.us-east-1.amazonaws.com/graphql"
});

module.exports = Config;
