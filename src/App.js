import React from "react";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import Config from "./Config";
import Routes from "./routes/Routes";

// Setup Apollo
const client = new ApolloClient({
  uri: Config.apiUri,
  headers: { "x-api-key": "da2-lxftgw43gfhkjeoklcj3sdzsxi" }
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Routes />
    </ApolloProvider>
  );
};

export default App;
